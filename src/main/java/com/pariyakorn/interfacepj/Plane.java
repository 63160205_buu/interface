/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pariyakorn.interfacepj;

/**
 *
 * @author acer
 */
public class Plane extends Vehicle implements Flyable,Runable{

    public Plane() {
        super("Plane");
    }

    @Override
    public void startEngine() {
        System.out.println("Plane: Start Engine.");
    }

    @Override
    public void stopEngine() {
        System.out.println("Plane: Stop Engine.");
    }

    @Override
    public void fly() {
        System.out.println("Plane: Fly.");
    }

    public void run() {
        System.out.println("Plane: Run.");
    }
}

    

