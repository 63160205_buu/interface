/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pariyakorn.interfacepj;

/**
 *
 * @author acer
 */
public class TestProgram {
    
    public static void main(String[] args) {
        Crocodile crocodile = new Crocodile("Mumoo");
        Snake snake = new Snake("Sin");
        Human human = new Human("Jeen");
        Dog dog = new Dog("Boo");
        Cat cat = new Cat("King");
        Fish fish = new Fish("Srisamor");
        Crab crab = new Crab("Maree");
        Bat bat = new Bat("PiTi");
        Bird bird = new Bird("Mana");
        Car car = new Car();
        Plane plane = new Plane();

        Crawlable[] crawlable = {crocodile, snake};
        for (Crawlable c : crawlable) {
            if (c instanceof Reptile) {
                Reptile r = (Reptile) c;
                r.eat();
                r.walk();
                r.crawl();
                r.speak();
                r.sleep();
            }
        }
        
        System.out.println("");
        
        Runable[] runable = {human, dog, cat, car, plane};
        for (Runable r : runable) {
            if (r instanceof LandAnimal) {
                LandAnimal l = (LandAnimal) r;
                l.eat();
                l.walk();
                l.run();
                l.speak();
                l.sleep();
            } else {
                if (r instanceof Car) {
                    Car c = (Car) r;
                    c.startEngine();
                    c.run();
                    c.stopEngine();
                } else if (r instanceof Plane) {
                    Plane p = (Plane) r;
                    p.startEngine();
                    p.run();
                    p.stopEngine();
                }
            }
        }
        
        System.out.println("");
        
        Swimmable[] swimmable = {fish, crab};
        for (Swimmable s : swimmable) {
            if (s instanceof AquaticAnimal) {
                AquaticAnimal a = (AquaticAnimal) s;
                a.eat();
                a.walk();
                a.swim();
                a.speak();
                a.sleep();
            }
        }
        
        System.out.println("");
        
        Flyable[] flyable = {bat, bird, plane};
        for (Flyable f : flyable) {
            if (f instanceof Poultry) {
                Poultry p = (Poultry) f;
                p.eat();
                p.walk();
                p.fly();
                p.speak();
                p.sleep();
            } else if (f instanceof Plane) {
                Plane p = (Plane) f;
                p.startEngine();
                p.run();
                p.stopEngine();
            }
        }
    }

}
